package com.example.pipux_v2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * CRUD de la balade
 */
public class CRUD_Balade {

    private static final int VERSION_BDD = 2;

    private static final String NOM_BDD = "pipux.db";

    private static final String TABLE_BALADE = "Balade";
    private static final String TABLE_POINT = "Point";

    private static final String COL_ID_BALADE = "id_balade";
    private static final String COL_NOM_BALADE = "nom_balade";

    private static final String COL_ID_POINT = "id_point";
    private static final String COL_NOM_POINT = "nom_point";
    private static final String COL_LATITUDE = "lat_point";
    private static final String COL_LONGITUDE = "long_point";
    private static final String COL_ID_BALADE_POINT = "id_balade_point";

    private SQLiteDatabase bdd;

    private Database db;

    /**
     * Permet d'initialiser la base de donner
     * @param context qui est le contexte actuel
     */
    public CRUD_Balade(Context context){
        db = new Database(context, NOM_BDD, null, VERSION_BDD);
    }

    /**
     * Permet de lire dans la base de donnée
     */
    public void open(){
        bdd = db.getWritableDatabase();
    }

    /**
     * Permet de fermer la base de donnée
     */
    public void close(){
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    /**
     * Permet d'inserer une balade dans la bdd
     * @param balade correspond a la balade que l'on souhaite insérer
     */
    public void insertBalade(Balade balade){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values_points = new ContentValues();
        ContentValues values_balade = new ContentValues();

        ArrayList<Point> position_points = balade.getPosition_points();

        //values_balade.put(COL_ID_BALADE, balade.getId_balade());
        values_balade.put(COL_NOM_BALADE, balade.getNom_balade());

        for (Point pos: position_points){
            values_points.put(COL_NOM_POINT, pos.getNom_point());
            values_points.put(COL_LATITUDE, pos.getLatitude_point());
            values_points.put(COL_LONGITUDE, pos.getLongitude_point());
            values_points.put(COL_ID_BALADE_POINT, balade.getId_balade());
        }

        bdd.insert(TABLE_BALADE, null, values_balade);
        bdd.insert(TABLE_POINT, null, values_points);

        return;
    }

    /**
     * Update de la bdd, permet de changer les valeurs de la balade
     * @param id correspond a l'id de la balade
     * @param balade correspond à la balade à modifier
     */
    public void updateBalade(int id, Balade balade){
        ContentValues values_points = new ContentValues();
        ContentValues values_balade = new ContentValues();

        ArrayList<Point> position_points = balade.getPosition_points();

        values_balade.put(COL_NOM_BALADE, balade.getNom_balade());

        for (Point pos: position_points){
            values_points.put(COL_NOM_POINT, pos.getNom_point());
            values_points.put(COL_LATITUDE, pos.getLatitude_point());
            values_points.put(COL_LONGITUDE, pos.getLongitude_point());
            values_points.put(COL_ID_BALADE_POINT, balade.getId_balade());
        }

        bdd.update(TABLE_BALADE, values_balade, COL_ID_BALADE + " = " + id, null);
        bdd.update(TABLE_POINT, values_points, COL_ID_BALADE_POINT + " = " + id, null);

        return;
    }

    /**
     * Permet de supprimer une balade
     * @param id correspond à l'id de la balade a supprimer
     * @return 0 ou 1
     */
    public int deleteBalade(int id){
        return bdd.delete(TABLE_BALADE, COL_ID_BALADE + " = " +id, null);
    }

    /**
     * Permet de récuperer une balade avec un id
     * @param id correspond à l'id de la balade à récuperer
     * @return la balade correpondant à l'id
     */
    public Balade getBaladeById(int id){
        String MY_QUERY = "SELECT * FROM " + TABLE_BALADE + " WHERE id_balade=?";
        Cursor c = bdd.rawQuery(MY_QUERY, new String[]{String.valueOf(id)});
        Log.d("STATUS", c.toString());
        return cursorBalade(c);
    }

    /**
     * Permet de récuperer toute les balades
     * @return une ArrayList de toutes les balades de la bdd
     */
    public ArrayList<Balade> getAllBalades(){
        Cursor cursor_balade = null;
        Cursor cursor_point = null;


        String query_balade = "SELECT * FROM " + TABLE_BALADE;
        String query_point = "SELECT * FROM " + TABLE_POINT;

        cursor_balade = bdd.rawQuery(query_balade,null);
        cursor_point = bdd.rawQuery(query_point,null);

        cursor_balade.moveToFirst();
        cursor_point.moveToFirst();

        ArrayList<Balade> list =new ArrayList<Balade>();

        while(cursor_balade.moveToNext()){
            boolean first = true;
            Balade row=new Balade();
            Point point_depart = new Point();
            ArrayList<Point> points = new ArrayList<>();

            row.setId_balade(cursor_balade.getInt(0));
            row.setNom_balade(cursor_balade.getString(1));

            while (cursor_point.moveToNext()){
                if (cursor_balade.getInt(0) == cursor_point.getInt(4)){
                    if (first){
                        point_depart.setId_point(cursor_point.getInt(0));
                        point_depart.setNom_point(cursor_point.getString(1));
                        point_depart.setLatitude_point(cursor_point.getDouble(2));
                        point_depart.setLongitude_point(cursor_point.getDouble(3));
                        point_depart.setId_point_balade(cursor_point.getInt(4));
                        points.add(point_depart);
                        first = false;
                    }
                    else {
                        Point tmp = new Point();
                        tmp.setId_point(cursor_point.getInt(0));
                        tmp.setNom_point(cursor_point.getString(1));
                        tmp.setLatitude_point(cursor_point.getDouble(2));
                        tmp.setLongitude_point(cursor_point.getDouble(3));
                        tmp.setId_point_balade(cursor_point.getInt(4));
                        points.add(tmp);
                    }
                }
            }
            cursor_point.close();
            row.setPosition_depart(point_depart);
            row.setPosition_points(points);
            list.add(row);
        }

        cursor_balade.close();
        db.close();
        return list;
        }

    /**
     * Permet de cast un cursor en une balade
     * @param c correspond au curseur
     * @return la balade
     */
    private Balade cursorBalade(Cursor c){
        if (c.getCount() == 0)
            return null;

        boolean first = true;
        Cursor cursor_point;
        String query_point = "SELECT id_point, nom_point, lat_point, long_point, id_balade_point, count(id_point) FROM " + TABLE_POINT;
        cursor_point = bdd.rawQuery(query_point,null);

        c.moveToFirst();

        Balade row = new Balade();
        Point point_depart = new Point();
        ArrayList<Point> points = new ArrayList<>();

        row.setId_balade(c.getInt(0));
        row.setNom_balade(c.getString(1));

        while (c.moveToNext()){
            if (c.getInt(0) == cursor_point.getInt(4)){
                if (first){
                    point_depart.setId_point(cursor_point.getInt(0));
                    point_depart.setNom_point(cursor_point.getString(1));
                    point_depart.setLatitude_point(cursor_point.getDouble(2));
                    point_depart.setLongitude_point(cursor_point.getDouble(3));
                    point_depart.setId_point_balade(cursor_point.getInt(4));
                    points.add(point_depart);
                    first = false;
                }
                else {
                    Point tmp = new Point();
                    tmp.setId_point(cursor_point.getInt(0));
                    tmp.setNom_point(cursor_point.getString(1));
                    tmp.setLatitude_point(cursor_point.getDouble(2));
                    tmp.setLongitude_point(cursor_point.getDouble(3));
                    tmp.setId_point_balade(cursor_point.getInt(4));
                    points.add(tmp);
                }
            }
        }
        cursor_point.close();
        c.close();

        row.setPosition_depart(point_depart);
        row.setPosition_points(points);

        return row;
    }
}

