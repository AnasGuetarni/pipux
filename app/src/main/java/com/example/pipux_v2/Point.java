package com.example.pipux_v2;

import java.util.Objects;

public class Point {
    private int id_point;
    private String nom_point;
    private Double latitude_point;
    private Double longitude_point;
    private int id_point_balade;

    public int getId_point() {
        return id_point;
    }

    public void setId_point(int id_point) {
        this.id_point = id_point;
    }

    public String getNom_point() {
        return nom_point;
    }

    public void setNom_point(String nom_point) {
        this.nom_point = nom_point;
    }

    public Double getLatitude_point() {
        return latitude_point;
    }

    public void setLatitude_point(Double latitude_point) {
        this.latitude_point = latitude_point;
    }

    public Double getLongitude_point() {
        return longitude_point;
    }

    public void setLongitude_point(Double longitude_point) {
        this.longitude_point = longitude_point;
    }

    public int getId_point_balade() {
        return id_point_balade;
    }

    public void setId_point_balade(int id_point_balade) {
        this.id_point_balade = id_point_balade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point)) return false;
        Point point = (Point) o;
        return id_point == point.id_point &&
                id_point_balade == point.id_point_balade &&
                Objects.equals(nom_point, point.nom_point) &&
                Objects.equals(latitude_point, point.latitude_point) &&
                Objects.equals(longitude_point, point.longitude_point);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id_point, nom_point, latitude_point, longitude_point, id_point_balade);
    }

    @Override
    public String toString() {
        return "Point{" +
                "id_point=" + id_point +
                ", nom_point='" + nom_point + '\'' +
                ", latitude_point=" + latitude_point +
                ", longitude_point=" + longitude_point +
                ", id_point_balade=" + id_point_balade +
                '}';
    }

    public Point() {}

    public Point(String nom_point, Double latitude_point, Double longitude_point) {
        this.nom_point = nom_point;
        this.latitude_point = latitude_point;
        this.longitude_point = longitude_point;
    }

    public Point(String nom_point, Double latitude_point, Double longitude_point, int id_point_balade) {
        this.nom_point = nom_point;
        this.latitude_point = latitude_point;
        this.longitude_point = longitude_point;
        this.id_point_balade = id_point_balade;
    }
}
