package com.example.pipux_v2;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;


public class Map extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    final static int PERMISSION_ALL = 1;
    final static String[] PERMISSIONS = {android.Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};
    private TextView mTextMessage;
    private GoogleMap mMap;

    private Balade balade;
    private boolean is_new = true;
    private CRUD_Balade crud_balade = new CRUD_Balade(this);

    private Button start_balade;
    private Button end_balade;

    private boolean running_balade = false;

    private Location my_loc;
    private LocationRequest locationRequest;
    private FusedLocationProviderClient fusedLocationProviderClient;

    MarkerOptions mo;
    Marker marker;

    LocationManager locationManager;

    ArrayList<MarkerOptions> list_markers = new ArrayList<>();
    ArrayList<Point> list_points = new ArrayList<>();
    Point position_depart;

    Polyline line;

    /**
     * Importe les markers de la balade
     * @param balade correspond a la balade cliqué sur la liste des balades
     */
    private void import_markers(Balade balade) {
        if (balade != null){
            String nom_balade = balade.getNom_balade();
            Point point_depart = balade.getPosition_depart();
            ArrayList<Point> list_p = balade.getPosition_points();

            for (Point b: list_p){
                MarkerOptions tmp = new MarkerOptions().position(new LatLng(b.getLatitude_point(), b.getLongitude_point())).title(b.getNom_point());
                mMap.addMarker(tmp);
                list_markers.add(tmp);
            }
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //mTextMessage.setText(R.string.title_home);
                    Context context = getApplicationContext();

                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                    return true;
                case R.id.navigation_dashboard:
                    //mTextMessage.setText(R.string.title_dashboard);
                    return true;
            }
            return false;
        }
    };

    /**
     * Création de map
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Intent intent = getIntent();

        addListenerOnButton();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(2500);

        crud_balade.open();

        if (intent != null){
            int position = -1;
            if (intent.hasExtra("pos")){
                position = intent.getIntExtra("pos", 0);
                if (position != -1){
                    balade = crud_balade.getBaladeById(position+1);
                    import_markers(balade);
                    is_new = false;
                }
            }
        }

        mo = new MarkerOptions().position(new LatLng(0, 0)).title("My Current Location");
        if (Build.VERSION.SDK_INT >= 23 && !isPermissionGranted()) {
            requestPermissions(PERMISSIONS, PERMISSION_ALL);
        } else requestLocation();
        if (!isLocationEnabled())
            showAlert(1);
    }

    /**
     * Permet de démarrer et arreter une balade
     */
    private void addListenerOnButton() {
        final Context context = getApplicationContext();

        start_balade = findViewById(R.id.button2);
            end_balade= findViewById(R.id.button3);

        start_balade.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                CharSequence text = "Partons pour une nouvelle balade !";
                int duration = Toast.LENGTH_SHORT;

                Toast.makeText(context, text, duration).show();

                running_balade = true;
            }

        });

        end_balade.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                boolean first = true;

                if (list_markers.size() > 1){
                    for (MarkerOptions m: list_markers){
                        if (first) {
                            Log.d("STATUS", m.getTitle());
                            if (!is_new)
                                position_depart = new Point(m.getTitle(), m.getPosition().latitude, m.getPosition().longitude, balade.getId_balade());
                            else
                                position_depart = new Point(m.getTitle(), m.getPosition().latitude, m.getPosition().longitude);
                            Log.d("STATUS", position_depart.toString());
                            balade.setPosition_depart(position_depart);
                            first = false;
                        }
                        else {
                            Point tmp;
                            if (!is_new)
                                tmp = new Point(m.getTitle(), m.getPosition().latitude, m.getPosition().longitude, balade.getId_balade());
                            else
                                tmp = new Point(m.getTitle(), m.getPosition().latitude, m.getPosition().longitude);

                            list_points.add(tmp);
                        }
                    }

                    balade.setPosition_points(list_points);

                    if (!is_new)
                        crud_balade.updateBalade(balade.getId_balade(), balade);
                    else
                        crud_balade.insertBalade(balade);

                    CharSequence text_final = "Fin de la balade !";
                    int duration = Toast.LENGTH_SHORT;

                    Toast.makeText(context, text_final, duration).show();

                    running_balade = false;

                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                }
                else {
                    CharSequence text_final = "Votre chemin ne constitue pas une balade !";
                    int duration = Toast.LENGTH_SHORT;

                    Toast.makeText(context, text_final, duration).show();

                    return;
                }

            }
        });
    }

    /**
     * Une fois que la map est chargé, on charge notre position
     * @param googleMap correspond a la Map
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        marker =  mMap.addMarker(mo);
        list_markers.add(mo);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 15));
        set_position();
        marker_add();
    }

    private void set_position() {
        try {
            Task locationResult = fusedLocationProviderClient.getLastLocation();
            locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        Log.d("STATUS", task.getResult().toString());
                        my_loc = (Location) task.getResult();
                    } else {
                        Log.d("loc_error", "Current location is null. Using defaults.");
                        Log.e("loc_error", "Exception: %s", task.getException());
                        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_LOCATION, DEFAULT_ZOOM));
                        //mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                }
                });

        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
     * Quand la position change on récupère les coordonnées
     * @param location correpond a la position courante
     */
    @Override
    public void onLocationChanged(Location location) {
        LatLng myCoordinates = new LatLng(location.getLatitude(), location.getLongitude());
        marker.setPosition(myCoordinates);
        mo.position(myCoordinates);
        list_markers.add(mo);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(myCoordinates));
    }

    /**
     * Création d'un marker sur la map au click
     */
    public void marker_add(){
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions my_mark = new MarkerOptions();
                MarkerOptions last_marker = list_markers.get(list_markers.size() - 1);
                my_mark.position(latLng);

                if (my_mark.getPosition() != null){
                    my_mark.title(latLng.latitude + "/" + latLng.longitude);
                    list_markers.add(my_mark);

                    Point p = new Point(latLng.latitude + "/" + latLng.longitude, latLng.latitude, latLng.longitude);
                    list_points.add(p);

                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.addMarker(my_mark);


                    line = mMap.addPolyline(new PolylineOptions()
                        .add(latLng, last_marker.getPosition())
                        .width(5)
                        .color(Color.BLUE));
                }
            }
        });
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @SuppressLint("MissingPermission")
    private void requestLocation() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_HIGH);
        String provider = locationManager.getBestProvider(criteria, true);
        locationManager.requestLocationUpdates(provider, 10000, 10, this);
    }
    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @SuppressLint("NewApi")
    private boolean isPermissionGranted() {
        if (checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED || checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Log.v("mylog", "Permission is granted");
            return true;
        } else {
            Log.v("mylog", "Permission not granted");
            return false;
        }
    }

    /**
     * Confirme l'utilisation de la Géolocalisation
     * @param status
     */
    private void showAlert(final int status) {
        String message, title, btnText;
        if (status == 1) {
            message = "Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                    "use this app";
            title = "Enable Location";
            btnText = "Location Settings";
        } else {
            message = "Please allow this app to access location!";
            title = "Permission access";
            btnText = "Grant";
        }
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton(btnText, new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        if (status == 1) {
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                        } else
                            requestPermissions(PERMISSIONS, PERMISSION_ALL);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        finish();
                    }
                });
        dialog.show();
    }




}
