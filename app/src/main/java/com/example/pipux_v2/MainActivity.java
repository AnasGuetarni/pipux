package com.example.pipux_v2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Activity principale
 * Liste les différentes balades dans une ListView
 * Permet de rejoindre une ancienne balade
 * Permet de créer une nouvelle balade
 */
public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    Button button;
    ListView mListView;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    Context context = getApplicationContext();

                    Intent intent = new Intent(context, Map.class);
                    startActivity(intent);
                    return true;
            }
            return false;
        }
    };

    /**
     * Lancé au lancement de l'application
     * Permet d'initialiser la liste des balades
     * Permet d'initialiser les différents boutons accessible sur la page
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextMessage = (TextView) findViewById(R.id.message);
        mListView = (ListView) findViewById(R.id.list_elements);

        addListenerOnButton();
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        CRUD_Balade crud_balade = new CRUD_Balade(this);
        crud_balade.open();

        ArrayList<Point> list_points = new ArrayList<>();
        Point vue_coucher_soleil = new Point("Coucher soleil", 10.0, 10.0);
        Point vue_rouge = new Point("Ciel rouge", 15.0, 15.0);
        list_points.add(vue_rouge);
        list_points.add(vue_coucher_soleil);

        crud_balade.insertBalade(new Balade("Footage", vue_coucher_soleil, list_points));

        ArrayList<Balade> list_balades = crud_balade.getAllBalades();

        Log.d("STATUS", String.valueOf(list_balades.isEmpty()));

        final ArrayAdapter<Balade> adapter = new ArrayAdapter<Balade>(MainActivity.this,
                android.R.layout.simple_list_item_1, list_balades);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Context context = getApplicationContext();
                Intent intent = new Intent(context, Map.class);
                intent.putExtra("pos", position);
                startActivity(intent);
            }
        });

        crud_balade.close();
    }

    /**
     * Au clic du bouton "Partir en tournée" permet de rediriger vers la map
     */
    public void addListenerOnButton() {
        button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Context context = getApplicationContext();

                Intent intent = new Intent(context, Map.class);
                startActivity(intent);

            }

        });

    }

}
