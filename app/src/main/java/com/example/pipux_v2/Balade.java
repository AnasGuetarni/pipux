package com.example.pipux_v2;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Classe Balade
 */
public class Balade {
    private int id_balade;
    private String nom_balade;
    private Point position_depart;
    private ArrayList<Point> position_points;

    public int getId_balade(){
        return id_balade;
    }

    public void setId_balade(int id_balade){
        this.id_balade = id_balade;
    }

    public String getNom_balade() {
        return nom_balade;
    }

    public void setNom_balade(String nom_balade) {
        this.nom_balade = nom_balade;
    }

    public Point getPosition_depart() {
        return position_depart;
    }

    public void setPosition_depart(Point position_depart) {
        this.position_depart = position_depart;
    }

    public ArrayList<Point> getPosition_points() {
        return position_points;
    }

    public void setPosition_points(ArrayList<Point> position_points) {
        this.position_points = position_points;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Balade)) return false;
        Balade balade = (Balade) o;
        return getId_balade() == balade.getId_balade() &&
                Objects.equals(getNom_balade(), balade.getNom_balade()) &&
                Objects.equals(getPosition_depart(), balade.getPosition_depart()) &&
                Objects.equals(getPosition_points(), balade.getPosition_points());
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(getId_balade(), getNom_balade(), getPosition_depart(), getPosition_points());
    }

    /**
     * Permet d'afficher la balade
      * @return la balade
     */

    @Override
    public String toString() {
        return "Balade{" +
                "id_balade=" + id_balade +
                ", nom_balade='" + nom_balade + '\'' +'}';
    }

    public Balade(){}

    /**
     * Constructeur d'une balade
     * @param nom_balade correspond au nom de la balade
     * @param position_depart correspond a la position de depart de la balade
     * @param position_points correspond a la position des différents points de la balade
     */
    public Balade(String nom_balade, Point position_depart, ArrayList<Point> position_points){
        this.nom_balade = nom_balade;;
        this.position_depart = position_depart;
        this.position_points = position_points;
    }

}
