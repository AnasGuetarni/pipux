package com.example.pipux_v2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class Database extends SQLiteOpenHelper {

    private static final String TABLE_BALADE = "Balade";
    private static final String TABLE_POINT = "Point";

    public Database(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * Création des tables dans la bdd
     * @param db correspond à la bdd
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Balade (id_balade INTEGER PRIMARY KEY AUTOINCREMENT, nom_balade TEXT NOT NULL)");
        db.execSQL("CREATE TABLE Point (id_point INTEGER PRIMARY KEY AUTOINCREMENT, nom_point TEXT NOT NULL, lat_point TEXT NOT NULL, long_point TEXT NOT NULL, id_balade_point INTEGER NOT NULL, CONSTRAINT fk_balade_point FOREIGN KEY (id_balade_point) REFERENCES Balade(id_balade))");
    }

    /**
     * Supprime et recrée une nouvelle bdd clean
     * @param db correspond a la bdd
     * @param oldVersion correspond a l'ancien numero de version de la BDD
     * @param newVersion correspond au nouveau numéro de version de la BDD
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BALADE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_POINT);
        onCreate(db);
    }
}
